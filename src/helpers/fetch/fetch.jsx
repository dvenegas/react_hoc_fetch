import React from 'react';
import axios from 'axios';
import isEmpty from 'lodash/isEmpty';
import isUndefined from 'lodash/isUndefined';

const FetchHOC = (source, params) => (
    Component => class fetch extends React.Component {
        state = {
            data: [],
            loading: true.toString(),
            error: false.toString(),
            empty: false.toString(),
            description: {}
        }

        componentDidMount = () => {
            console.log(this.props);
            if (isUndefined(source)) {
                this.fetchData(this.props.url);
                return;
            }
            this.fetchData(source);
        }

        values = {
            true: true.toString(),
            false: false.toString()
        }

        fetchData = (url) => {
            if (!url) return;

            this.setState(this.initalState);

            axios(url, params)
                .then(({
                    data,
                    status,
                    statusText
                }) => {
                    if (status >= 400 && status <= 599) {
                        this.setState(() => ({
                            data,
                            error: this.values.true,
                            description: new Error(statusText),
                            loading: this.values.false,
                            empty: this.values.false
                        }));
                        return;
                    }

                    if (isEmpty(data)) {
                        this.setState(() => ({
                            data,
                            error: this.values.false,
                            empty: this.values.true,
                            loading: this.values.false
                        }));
                        return;
                    }
                    this.setState(() => ({
                        data,
                        loading: this.values.false,
                        error: this.values.false,
                        empty: this.values.false
                    }), console.log("todo chingon"));
                    
                })
                .catch((error) => {
                    this.setState(() => ({
                        data: [],
                        error: this.values.true,
                        description: error,
                        loading: this.values.false,
                        empty: this.values.false
                    }));
                });
        }

        render() {
            return <Component {...this.props} {...this.state} />;
        }
    }
);

export default FetchHOC;
