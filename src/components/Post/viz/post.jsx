import React from 'react';

const VizPost = ({ item: { title } }) => <div>{title}</div>;

export default VizPost;
