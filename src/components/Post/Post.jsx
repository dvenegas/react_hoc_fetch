import React from 'react';
import PropTypes from 'prop-types';

import Post from './viz';
import FetchHOC from '../../helpers/fetch';

const propTypes = {
    data: PropTypes.array, // eslint-disable-line
    loading: PropTypes.string,
    error: PropTypes.string,
    empty: PropTypes.string,
    description: PropTypes.object // eslint-disable-line
};

const defaultProps = {
    data: [],
    loading: '',
    error: '',
    empty: '',
    description: {}
};

const StatusComponent = ({
    data, loading, error, empty, description, url
}) => {
    const convertToBool = string => (string === 'true');
    if (convertToBool(loading)) return <div>cargando</div>;
    if (convertToBool(error)) return <div>{description.toString()}</div>;
    if (convertToBool(empty)) return <div>empty</div>;
    return data.map(post => <Post key={post.id} item={post} url={url} />);
};

StatusComponent.propTypes = propTypes;
StatusComponent.defaultProps = defaultProps;
StatusComponent.displayName = "component";

export default FetchHOC()(StatusComponent);
