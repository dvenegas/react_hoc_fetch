import React from 'react';
import logo from './logo.svg';
import Post from './components/Post';
import './App.css';

const App = () => (
    <div className="App">
        <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
        </header>
        <main>
            <Post url="https://jsonplaceholder.typicode.com/posts" />
        </main>
    </div>
);

export default App;
